*** Variables ***
### System Variables ###

${TESTS_DIR}   tests
# ${GITLAB_API_VERSION}   v4
# ${GITLAB_USERNAME}   fxs
# ${GITLAB_NAME}   jfxs
${GRID_URL}   http://selenium:4444/wd/hub
${BROWSER}   Chrome
${RF_SENSITIVE_VARIABLE}   ${EMPTY}

### Sign In ###

${username} =                 //input[@formcontrolname='username']
${password} =                 //input[@formcontrolname='password']
${SignIn} =                   //span[@class='mat-button-wrapper']


### Sign Up ###

${signUp}                      //a[normalize-space()='Sign up']
${orgName}                     //input[@formcontrolname='organisation']  
${subDomain}                   //input[@formcontrolname='subdomain'] 
${nextStep}                    //span[normalize-space()='Next step']

${userName}                    //input[@formcontrolname='username'] 
${firstName}                   //input[@formcontrolname='firstName'] 
${lastName}                    //input[@formcontrolname='lastName'] 
${signEmail}                   //input[@formcontrolname='email'] 
${signPassword}                //input[@formcontrolname='password'] 
${repeatPassword}              //input[@formcontrolname='repeatPassword'] 
${signPassword}                TestingQA123

${checkboxAgree}               //div[@class='mat-checkbox-frame']
${Finish}                      //span[normalize-space()='Finish']
                                    
### Add Company ###

${addKnowledge} =             //a[normalize-space()='Add knowledge']
${akVideo} =                  //button[@mattooltip='Add video']
${adImages} =                 //mat-icon[normalize-space()='image'] 
${adAudio} =                  //button[@mattooltip='Add audio']
${adText} =                   //button[@mattooltip='Add text']
${file_path_variable}         robot/data/cover.jpg

### Ask ###

${lmAsk}                      //a[normalize-space()='Ask']

### Courses ###

${lmCourses}                  //a[normalize-space()='Courses']


### Add Courses ###

${lmAddCourses}                //a[normalize-space()='Add course']

*** Keywords ***

Open Sideways Application
    SeleniumLibrary.Open Browser	  https://dev.sideways.work/login  ${BROWSER} 	remote_url=${GRID_URL}
    Sleep   3s
    #remote_url=${GRID_URL}
Sign In With Valid Email
    Click Element   ${username}
    SeleniumLibrary.Input Text      ${username}     milan.novovic@equaleyes.com
    Click Element   ${SignIn}
    SeleniumLibrary.Input Text      ${password}     Testingqa123
    Sleep   3s
    Click Element   ${SignIn}
    Maximize Browser Window

Enter valid email
    Click Element                   ${username}
    SeleniumLibrary.Input Text      ${username}     milan.novovic@equaleyes.com
    Sleep   2s

And password
    Click Element   ${SignIn}
    SeleniumLibrary.Input Text      ${password}     Testingqa123

And Click on Sign In button
    Sleep   3s
    Click Element   ${SignIn}
    Maximize Browser Window


Then Home page will be presented
    Sleep   3s
    Click Element    //img[@class='mat-menu-trigger']
    Sleep   3s
    Page Should Contain Element    //div[@class='cdk-overlay-container']//button[1]
    Click Element    //span[normalize-space()='Profile']

And Click on Log Out Button 
    Sleep   3s
    Click Element    //img[@class='mat-menu-trigger']
    Sleep   3s
    Click Element   //span[normalize-space()='Log out']

Then Sign In Button Should Be Present
    Sleep   3s
    Page Should Contain Element    ${SignIn}  

Click on Sign Up button

    Maximize Browser Window
    Click Element     ${signUp}
    Sleep  3s

Enter OrgName and SubDomain values

    ${orgNameText}          FakerLibrary.Company 
    ${subDomainText}        FakerLibrary.Domain Name

    Input Text        ${orgName}        ${orgNameText} 
    Sleep  3s
    Input Text        ${subDomain}      ${subDomainText}
    Sleep  3s
    Click Element     //span[normalize-space()='Next step']

Enter Valid Profile Registration Data
    
    ${userNameData}         FakerLibrary.First Name 
    ${firstNameData}        FakerLibrary.First Name
    ${lastNameData}         FakerLibrary.Last Name 
    ${emailData}            FakerLibrary.Email


    Sleep  3s
    Input Text      ${userName}         ${userNameData}         
    Sleep  3s
    Input Text      ${firstName}        ${firstNameData}  
    Sleep  3s
    Input Text      ${lastName}         ${lastNameData}
    Sleep  3s
    Input Text      ${signEmail}        ${emailData}  
    Sleep  3s
    Input Text      ${signPassword}     TestingQA123   
    Sleep  3s
    Input Text      ${repeatPassword}   TestingQA123
    Sleep  3s
Click on T&C checkbox
    Page Should Contain Element     //*[@class='mat-checkbox-inner-container']
    Mouse Over                      //*[@class='mat-checkbox-inner-container']
    Sleep  3s
    Click Element                   //*[@class='mat-checkbox-inner-container']
    Sleep  3s
Click on Finish Button
    Mouse Over                      //*[contains(@class,'mat-button-wrapper') and contains(text(),' Finish ')]
    Sleep  3s
    Click Element                   //*[contains(@class,'mat-button-wrapper') and contains(text(),' Finish ')]
    Sleep  3s
Then Sign In button will appear
    Page Should Contain Element     //*[contains(@class,'mat-button-wrapper') and contains(text(),' Sign In')]
    Mouse Over                      //*[contains(@class,'mat-button-wrapper') and contains(text(),' Sign In')]
    Sleep  3s    
    Click Element                   //*[contains(@class,'mat-button-wrapper') and contains(text(),' Sign In')]
And Sideways Logo Will appear   
    Page Should Contain Element    ${SignIn}         

And Click on Ask page   
    Sleep  3s 
    Click Element           //div[contains(@class,'mat-list-text')]//*[text() = 'Ask']

And Enters the Question
    Sleep  3s 
    Input Text             //*[@formcontrolname='questionText']      What is Sideways?
    Sleep  3s 
    Click Element           //span[normalize-space()='Ask']
    Sleep  3s 
    ${value}   Get Text    //*[@class='question-text mt-1']

    Input Text             //*[@formcontrolname='questionText']     ${value}

Then User Can Delete Question
    Sleep  3s
    Mouse Over          (//*[@class='mat-button-wrapper'])[9]
    Click Element       (//*[@class='mat-button-wrapper'])[9]
    Sleep  3s
    Click Element       //*[@id='btnDelete']
    Sleep  3s
    Page Should Contain Element     //*[contains(text(),' Nobody has asked a question yet')]

Click on Add Knowledge
    Click Element   ${addKnowledge}
    Sleep   2s
    Click Element   ${adImages}  
    Sleep   5s

Upload Files to Drop zone
    File Should Exist   robot/data/cover.jpg
    Sleep   5s
    Page Should Contain Element         //app-file-select//input[@type='file']         
    Choose File         //app-file-select//input[@type='file']     robot/data/cover.jpg
    Sleep   2s
#    Win Kill           Open
#    Sleep   2s

    
Wait Until Upload is Finished
    Wait Until Element Is Visible   //span[contains(text(),' Uploading 100% ')]     15s  
    Page Should Contain Element     //span[contains(text(),' Uploading 100% ')]
    Sleep   2s

And Click on Next button    
    Click Element                   //span[normalize-space()='Next step']

And Enters Knowledge title and Short Description 
    Wait Until Element is Visible   //input[@placeholder='Knowledge title']
    Sleep   2s
    Input Text                       //input[@placeholder='Knowledge title']       Title of Tuttorial
    Wait Until Element is Visible   //*[@placeholder='Add a short description']
    Sleep   2s
    Input Text                       //*[@placeholder='Add a short description']   Lorem Ipsum     
    Sleep   2s

Then User will be enabled to Publish its Media
    Click Element                   //*[contains(@class,'mat-button-wrapper') and contains(text(),'Publish')]
    Sleep   2s
    Click Element                   //*[contains(@class,'mat-button-wrapper') and contains(text(),'Close')]
    Sleep   10s

And User Is On Bank Page
    Sleep   4s
    Click Element                  //div[contains(@class,'mat-list-text')]//*[text() = 'Bank']

And Click on Existing Media
    Sleep   2s
    Click Element                  //app-content-card[10]//mat-card[1]//mat-card-content[1]

And Delete Selected Media   
    Sleep   2s 
    Click Element                   //mat-icon[normalize-space()='delete_outline']
    Sleep   5s 
    Click Element                   //span[normalize-space()='Delete']



