*** Settings ***

Documentation  Sideways.Demo
Suite Teardown  Close Browser
Test Teardown   Close Browser
Library         SeleniumLibrary         
Library         String
Library         Collections  
Library         FakerLibrary
Library         OperatingSystem
Library         AutoItLibrary
Resource        keyword_resource.robot


*** Test Cases *** 

TC-1 Validate Log In To Application flow

    Open Sideways Application
    Enter valid email
    And password
    And Click on Sign In button
    Then Home page will be presented


TC-2 Validate Log Out from the Account flow
    Open Sideways Application
    Enter valid email
    And password
    And Click on Sign In button
    Then Home page will be presented
    And Click on Log Out Button
    Then Sign In Button Should Be Present

TC-3 Validate Sign Up with Valid Email Address flow

     Open Sideways Application
     Click on Sign Up button
     Enter OrgName and SubDomain values  
     Enter Valid Profile Registration Data
     Click on T&C checkbox
     Click on Finish Button
     Then Sign In button will appear   
     And Sideways Logo Will appear  

TC-4 Validate Ask and Delete option flow 

    Open Sideways Application
    Sign In With Valid Email
    Then Home page will be presented
    And Click on Ask page 
    And Enters the Question
    Then User Can Delete Question

# TC-5 Validate Add Knowledge section can upload files

#     Open Sideways Application
#     Sign In With Valid Email
#     Then Home page will be presented
#     Click on Add Knowledge
#     Upload Files to Drop zone
#     Wait Until Upload is Finished
#     And Click on Next button
#     And Enters Knowledge title and Short Description
#     Then User will be enabled to Publish its Media

TC-6 Validate that User can Delete Course from Bank
 
    Open Sideways Application
    Sign In With Valid Email
    Then Home page will be presented
    And User Is On Bank Page
    And Click on Existing Media
    And Delete Selected Media





